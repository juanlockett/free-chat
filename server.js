const express = require('express');
const app = express();
const server = require('http').Server(app);

const config=require('./config');

const bodyParser = require('body-parser');
const socket = require('./socket');
const db = require('./db');

//const router = require('./components/message/network');
const router = require('./network/routes');

db(config.dbUrl);




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

socket.connect(server);

router(app);

app.use('/app', express.static('public'));

server.listen(3000, function () {
    console.log('[server] OK: la aplicación esta escuchando en http://localhost:3000');
});
