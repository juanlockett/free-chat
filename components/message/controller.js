const store = require('./store');
const socket = require('../../socket').socket;

function addMessage(chat, user, message, file) {
    return new Promise((resolve, reject) => {
        if (!chat || !user || !message) {
            console.error('[messageController] No hay chat, usuario o mensaje');
            reject('Los datos son incorrectos');
            return false;
        }

        let fileUrl = '';
        if (file) {
            fileUrl = 'http://localhost:3000/app/files/' + file.filename;
        }

        const fullMessage = {
            chat: chat,
            user: user,
            message: message,
            date: new Date(),
            file: fileUrl,
        }

        store.add(fullMessage);

        socket.io.emit('message', fullMessage)

        resolve(fullMessage);
    })
}

function getMessages(filterUser) {
    return new Promise((resolve, reject) =>{
        resolve(store.list(filterUser));
    })
}

function updateMessager (id, message) {
    return new Promise(async(resolve, reject) => {
        if (!id || !message){
            reject('Invalid data');
            return false;
        }
        const result = await store.updateMessage(id, message);
        resolve(result);
    })
}

function deleteMessage (id) {
    console.log(id)
    return new Promise((resolve, reject) => {
        if (!id) {
            reject('Parametros con ID invalido');
            return false;
        }
        store.remove(id)
            .then(() => {
                resolve();
            })
            .catch(e => {
                reject(e);
            })
    })
}

module.exports = {
    addMessage,
    getMessages,
    updateMessager,
    deleteMessage,
};