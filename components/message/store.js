
const Model = require('./model')
//mongodb+srv://db_user_node:<BrC0Ic8xVFEB7FEU>@cluster0-vwveb.mongodb.net/test


function addMessage(message) {
    const myMessage = new Model(message);
    myMessage.save();
}

async function getMessage(filterUser) {
    return new Promise((resolve, reject) => {
        let filter = {}
        if (filterUser !== null) {
            filter = { user: filterUser };
        }
        Model.find(filter)
            .populate('user')
            .exec((error, populated) =>{
                if (error){
                    reject(error);
                    return false;
                }
                resolve(populated);
            });
    })
    
}

function deleteMessage(id) {
    return Model.deleteOne({
        _id: id
    });
}

async function updateMessage(id, message){
    const foundMessage = await Model.findOne({
        _id: id
    });

    foundMessage.message = message;
    const newMessage = await foundMessage.save();
    return newMessage;
}

module.exports = {
    add: addMessage,
    list: getMessage,
    updateMessage: updateMessage,
    remove: deleteMessage,
}