const store = require('./store');

function addChat(users){
    return new Promise((resolve, reject) => {
        if (!users || !Array.isArray(users)) {
            console.error('[chatController] No hay chat o no es un array');
            reject('Los datos son incorrectos');
            return false;
        }
        const chat = {
            users: users,
        }
        console.log('hasta aca todo ok');
        store.add(chat);
        resolve(chat);
    });
};

function listChats(userId){
    return new Promise((resolve, reject) => {
        resolve(store.list(userId));
    })
};


module.exports = {
    addChat,
    listChats,
};